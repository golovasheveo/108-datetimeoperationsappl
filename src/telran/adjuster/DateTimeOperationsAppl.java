package telran.adjuster;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateTimeOperationsAppl {
    public static void main ( String[] args ) {

        WorkingDays dayObj = new WorkingDays ( 0, new DayOfWeek[] { DayOfWeek.SATURDAY, DayOfWeek.SUNDAY } );
        System.out.println ( dayObj.adjustInto ( LocalDate.parse ( "2020-05-22" ) ).toString ( ) );


        displayTimezoneId ( "CaNada" );

        displayCompleteAge ( "1799-06-06" ); //ISO 8601
        displayCompleteAge ( "06/06/1799" );
        displayCompleteAge ( "06.06.1799" );
    }

    private static void displayCompleteAge ( String strBirthdate ) {
        LocalDate birthDate = parseDate ( strBirthdate );
        LocalDate endDate = LocalDate.now ( );

        long years = ChronoUnit.YEARS.between ( birthDate, endDate );
        endDate = endDate.minus ( years, ChronoUnit.YEARS );

        long months = ChronoUnit.MONTHS.between ( birthDate, endDate );
        endDate = endDate.minus ( months, ChronoUnit.MONTHS );

        long days = ChronoUnit.DAYS.between ( birthDate, endDate );

        System.out.printf ( "Years: %d Months: %d Days: %d \n", years, months, days
        );
    }


    public static LocalDate parseDate ( String dateStr ) {
        LocalDate res;
        try {
            res = LocalDate.parse ( dateStr, DateTimeFormatter.ofPattern ( "dd/MM/yyyy" ) );
        }
        catch ( Exception e ) {
            res = LocalDate.parse ( dateStr );
        }
        return res;

    }

    private static void displayTimezoneId ( String zonePattern ) {
        for (String timeZone : ZoneId.getAvailableZoneIds ( )) {
            if ( timeZone.toLowerCase ( ).contains ( zonePattern.toLowerCase ( ) + "/" ) ) {
                System.out.println ( timeZone );
            }
        }
    }
}