package telran.adjuster;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;

public class WorkingDays implements TemporalAdjuster {

    private DayOfWeek[] daysOff;
    private int         workingDays;

    public WorkingDays ( int workingDays, DayOfWeek[] daysOff ) {
        this.workingDays = workingDays;
        this.daysOff     = daysOff;
    }

    @Override
    public Temporal adjustInto ( Temporal temporal ) {
        int counter = 0;
        LocalDate res = (LocalDate) temporal;
        if ( daysOff.length == 7 ) {
            return null;
        }
        if ( daysOff.length == 0 ) {
            return ( res.plus ( workingDays, ChronoUnit.DAYS ) );
        }
        while (counter <= workingDays) {
            res = res.plus ( 1, ChronoUnit.DAYS );
            if ( !checkDayOff ( res ) ) {
                counter++;
            }
        }
        return res;
    }

    private boolean checkDayOff ( LocalDate res ) {
        for (DayOfWeek day : daysOff) {
            if ( day.getValue ( ) == res.getDayOfWeek ( ).getValue ( ) ) {
                return true;
            }
        }
        return false;
    }

}
